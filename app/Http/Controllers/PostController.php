<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Services\Contracts\RepositroyModelContact;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Post model Actions
     *
     * @var \App\Services\Contracts\RepositroyModelContact
     */
    protected $post;

    public function __construct(RepositroyModelContact $postRepositroy)
    {
        $this->post = $postRepositroy;
    }

    public function index()
    {
        return view('post_create');
    }

    public function create(CreatePostRequest $request)
    {
        $createNewPost = $this->post->save($request->post);

        if ($createNewPost) {
            return redirect('/')->with('status', 'Post created!');
        }

        return redirect('/posts')->with('status','Post don`t created!');
    }
}
