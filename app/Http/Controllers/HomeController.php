<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Contracts\RepositroyModelContact;

class HomeController extends Controller
{
    protected $post;

    public function __construct(RepositroyModelContact $postRepositroy)
    {
        $this->post = $postRepositroy;
    }

    public function index(Request $request)
    {
        $posts = $this->post->all($request->filters);
        $filters = $request->filters;
        return view('welcome', compact('posts', 'filters'));
    }
}
