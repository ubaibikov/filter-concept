<?php

namespace App\Services\Contracts;

interface RepositroyModelContact
{
    /**
     * Get all model rws
     *
     * @param null|array $filters Filter values
     * @return Model
     */
    public function all(?array $filters);

    /**
     * Get detail model rw
     *
     * @param int $id Rw identifier
     * @return Model
     */
    public function detail(int $id);

    /**
     * Save new rw
     *
     * @param array $data Rw data
     * @return boolean
     */
    public function save(array $data): bool;

    /**
     * Update rw
     *
     * @param array $id Rw identifier
     * @return boolean
     */
    public function update(int $id): bool;

    /**
     * Update rw
     *
     * @param array $id Rw identifier
     * @return boolean
     */
    public function destroy(int $id): bool;
}
