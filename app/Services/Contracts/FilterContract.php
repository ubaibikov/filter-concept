<?php

namespace App\Services\Contracts;

interface FilterContract
{
    public function handleFilters(array $filters, object $filter);
}
