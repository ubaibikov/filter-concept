<?php

namespace App\Repositories;

use App\Repositories\Filters\Filter;
use App\Services\Contracts\{RepositroyModelContact};

class PostRepository implements RepositroyModelContact
{
    use Filter;

    private $model;

    public function __construct(\App\Post $postModel)
    {
        $this->model = $postModel;
    }

    public function all(?array $filters)
    {
        $this->select();

        if ($filters) $this->filter($filters);

        return $this->model->paginate(5);
    }

    public function select()
    {
        return $this->model = $this->model::select($this->getFillable());
    }

    public function detail(int $id)
    {
        return;
    }

    public function save(array $data): bool
    {
        $createPost = $this->model::create($data);

        return (bool)$createPost ?? false;
    }

    public function update(int $id): bool
    {
        return true;
    }

    public function destroy(int $id): bool
    {
        return true;
    }

    public function multiDestroy(array $ids): bool
    {
        $deletePosts = $this->model::whereIn('id', $ids)->delete();

        return (bool)$deletePosts ?? false;
    }

    public function getFillable(): array
    {
        $items = ['id', 'title', 'author', 'description', 'private'];

        return $items;
    }

    protected function filter(array $filters)
    {
        $this->handleFilters($filters);
        return;
    }

    public function authorFilter(string $value)
    {
        return $this->model->where('author', 'like', "%$value%");
    }

    public function titleFilter(string $value)
    {
        return $this->model->where('title', 'like', "%$value%");
    }

    public function privateFilter(int $value)
    {
        return $this->model->where('private', '=', $value);
    }
}
