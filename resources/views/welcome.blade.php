@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col pl-5">
            <div class="shadow p-3 mb-5 bg-white rounded" id="filter">
                <h3>Filter by Posts:</h3>
                <form action="{{route('filters')}}" method="GET">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="validationDefault01">Author:</label>
                            <input type="text" class="form-control" id="validationDefault01" name="filters[author]"
                                value="{{$filters['author'] ?? null}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="validationDefault01">Title:</label>
                            <input type="text" class="form-control" id="validationDefault01" name="filters[title]"
                                value="{{$filters['title'] ?? null}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input " type="checkbox" value="1" name="filters[private]"
                                id="invalidCheck2" @if (!empty($filters['private']) && $filters['private']) checked
                                @endif>
                            <label class="form-check-label" for="invalidCheck2">
                                Private Post
                            </label>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Filter</button>
                </form>
            </div>
            <div class="shadow p-3 mb-5 bg-white rounded">
                <div class="row">
                    <div class="col">
                        <a class="btn w-100 btn-light" href="{{asset('/posts')}}">Create</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-8">
            @if ($posts->isEmpty())
            <p> Not Have Posts</p>
            @endif
            @foreach ($posts as $post)
            <div class="card shadow p-3 mb-5 bg-white rounded" style="">
                <div class="card-body">
                    <h5 class="card-title pb-1">Title: {{$post->title}}</h5>
                    <h6 class="card-subtitle mb-2 text-muted pb-1">Author: {{$post->author}}</h6>
                    <p class="card-text ">
                        @if ($post->private)
                        Private
                        @else
                        {{$post->description}}
                        @endif
                    </p>
                </div>
            </div>
            @endforeach

            {{$posts->appends(['filters' => $filters])->links() }}
        </div>
    </div>
    @endsection
