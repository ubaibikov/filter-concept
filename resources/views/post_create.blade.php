@extends('layouts.main')

@section('content')
<div class="container mt-5">
    <div class="card shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <h5 class="card-title pb-1">Create Post</h5>
            <form action="{{route('posts.create')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="titleInput">Title:</label>
                    <input type="text" class="form-control" id="titleInput" name="post[title]">
                </div>
                <div class="form-group">
                    <label for="AuthorTitle">Author:</label>
                    <input type="text" class="form-control" id="AuthorTitle" name="post[author]">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description:</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"
                        name="post[description]"></textarea>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" value="1" name="post[private]">
                    <label class="form-check-label" for="exampleCheck1">Private</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
